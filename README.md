# ics-ans-role-wazuh-server

Ansible role to install wazuh-server.

## Role Variables

```yaml
wazuh_upgrade: true
wazuh_version1: 4
wazuh_version2: "{{ wazuh_version1 }}.4"
wazuh_version3: "{{ wazuh_version2 }}.0"
filebeat_version: 0.2

wazuh_install_aio: "https://packages.wazuh.com/resources/{{ wazuh_version }}/open-distro/unattended-installation/unattended-installation.sh"
wazuh_notification: "yes"

# Email & alert settings
wazuh_smtp_server: smtp.localhost
wazuh_email_from: admin@localhost
wazuh_email_to: [] #list of email addresses
wazuh_alerts_log: 3 # minimum alert level to log and index
wazuh_alerts_email: 12 # minimum alert event to alert and send an email

# Example email groups
wazuh_email_groups:
    #see https://documentation.wazuh.com/current/user-manual/manager/manual-email-report/
  - type: none # group, rule_id, event_location 
    type_content: "somegroup,|someothergroup"
    email:
      - acct1@local
      - acct2@local

# LDAP settings
wazuh_ldap_host: ldap.host.com
wazuh_ldap_port: 636
wazuh_ldap_bind_dn: uid=admin,ou=it,dc=host,dc=com
wazuh_ldap_bind_password: changeme
wazuh_ldap_group_dn: cn=admins,ou=groups,dc=host,dc=com
wazuh_role_base: dc=host,dc=com

# Wazuh integration
wazuh_integration:
  - name: custom-jira
    group: ids
    url: https://somejirainstance.com
    key: JIRAPROJECT:APIKEY:/path/to/ignore_list
  - name: stackstorm
    group: ids
    url: https://stackstorm/api
    key: ACTION:APIKEY:/path/to/ignore_list
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-wazuh-server
```

## License

BSD 2-clause
---
