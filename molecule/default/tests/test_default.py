import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


timestamp = "2021-10-04T15:02:30.468Z"
logtest = "/var/ossec/bin/wazuh-logtest"
msg_body = "Failed Login: username=testuser ip=1.1.1.1"


def test_wazuh_manager(host):
    wazuh_service = {"wazuh-manager", "wazuh-dashboard", "wazuh-indexer"}
    for item in wazuh_service:
        service = host.service(item)
        assert service.is_running
        assert service.is_enabled


def test_services_are_listening(host):
    for port in ('443', '1514'):
        assert host.socket("tcp://" + port).is_listening


def test_tcp6_services_are_listening(host):
    cmd = host.run("curl http://localhost:9200")
    assert cmd.rc == 52


def test_logtest(host):
    cmd = host.run("echo '" + timestamp + ": " + msg_body + "' | sudo " + logtest)
    assert 'Alert to be generated' in cmd.stderr
